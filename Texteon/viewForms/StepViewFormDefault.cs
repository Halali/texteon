﻿using System.Windows.Forms;
using LogicModule;

namespace ViewModule.viewForms
{
    class StepViewFormDefault : Form, IStepViewForm
    {
        private FormInstanceTableLayout tableLayout;

        public StepViewFormDefault(int columns = 1, int rows = 1) : base()
        {
            this.FormBorderStyle = FormBorderStyle.None;
            tableLayout = new FormInstanceTableLayout(columns, rows);
            this.Controls.Add(tableLayout);
        }

        public void AssignControl(Control control)
        {
            tableLayout.Controls.Add(control);
        }

        public void ShowForm()
        {
            this.ShowDialog();
        }
    }

    internal class FormInstanceTableLayout : TableLayoutPanel
    {
        public FormInstanceTableLayout(int columns, int rows) : base()
        {
            this.ColumnCount = columns;
            this.RowCount = rows;
            this.AutoSize = true;
            this.Dock = DockStyle.Fill;
        }
    }
}
