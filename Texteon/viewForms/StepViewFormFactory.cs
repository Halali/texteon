﻿using static ViewModule.core.ControlEnums;

namespace ViewModule.viewForms
{
    class StepViewFormFactory
    {
        private static StepViewFormFactory viewFactory;

        private StepViewFormFactory()
        {
        }

        public static StepViewFormFactory Instance()
        {
            if (viewFactory != null)
            {
                return viewFactory;
            }
            else
            {
                return viewFactory = new StepViewFormFactory();
            }
        }

        public IStepViewForm CreateViewForm(ViewType viewType = ViewType.DefaultView)
        {
            switch (viewType)
            {
                case ViewType.DefaultView:
                    return new StepViewFormDefault();
                default:
                    return new StepViewFormDefault();
            }
        }

    }
}
