﻿using System.Windows.Forms;
using ViewModule.stepView;

namespace ViewModule.viewForms
{
    public interface IStepViewForm
    {
        void ShowForm();
        void AssignControl(Control control);
    }
}
