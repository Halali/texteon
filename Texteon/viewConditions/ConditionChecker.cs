﻿using System.Collections.Generic;
using System.Windows.Forms;
using ViewModule.viewConditions;

namespace LogicModule.instanceConditions
{
    public class ConditionChecker
    {
        private static ConditionChecker checker;
        public string FailMessage { get; set; }

        private ConditionChecker()
        {
        }

        public static ConditionChecker Instance()
        {
            if (checker != null)
            {
                return checker;
            }
            else
            {
                return checker = new ConditionChecker();
            }
        }

        public bool CheckConditions(List<IViewCondition> conditions)
        {
            if(conditions != null)
            {
                bool check = true;

                foreach (IViewCondition c in conditions)
                {
                    if (!c.CheckCondition())
                    {
                        MessageBox.Show(c.GetFailMessage());
                        c.SetFailMessage("");
                        check = false;
                    }
                }
                return check;
            }
            return true;
        }
    }
}
