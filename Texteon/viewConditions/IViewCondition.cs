﻿namespace ViewModule.viewConditions
{
    public interface IViewCondition
    {
        bool CheckCondition();
        string GetFailMessage();
        void SetFailMessage(string message);
    }
}
