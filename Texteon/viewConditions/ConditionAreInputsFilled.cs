﻿using System.Collections.Generic;
using ViewModule.controls;
using ViewModule.instanceEntities;

namespace ViewModule.viewConditions
{
    public class ConditionAreInputsFilled : IViewCondition
    {
        private IStepViewConteiner ViewConteiner { get; set; }
        private string FailMessage { get; set; }

        public ConditionAreInputsFilled(IStepViewConteiner viewConteiner)
        {
            ViewConteiner = viewConteiner;
        }

        public bool CheckCondition()
        {
            List<IInstanceViewControlInput> inputs = ViewConteiner.GetInputs();
            bool check = true;

            foreach (IInstanceViewControlInput i in inputs)
            {
                if (i.GetTextInput() == "" && i.GetIsMandatory())
                {
                    FailMessage += i.GetName() + " is mandatory!" + System.Environment.NewLine;
                    check = false;
                }
            }
            return check;
        }

        public string GetFailMessage()
        {
            return FailMessage;
        }

        public void SetFailMessage(string message)
        {
            FailMessage = message;
        }
    }
}
