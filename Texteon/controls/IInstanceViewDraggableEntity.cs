﻿using System.Drawing;
using ViewModule.controls;

namespace Dungeon
{
    interface IInstanceViewDraggableEntity : IInstanceViewControl
    {
        void SetDrag(bool dragged);
        bool GetDrag();
        Point PointToScreen(int x, int y);
    }
}
