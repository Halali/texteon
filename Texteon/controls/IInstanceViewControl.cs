﻿using System.Drawing;
using ViewModule.viewForms;

namespace ViewModule.controls
{
    public interface IInstanceViewControl
    {
        void SetText(string text);
        void SetLocation(Point point);
        Point GetLocation();
        void PutOnView(IStepViewForm view);
        string GetName();
        void SetName(string name);
    }
}
