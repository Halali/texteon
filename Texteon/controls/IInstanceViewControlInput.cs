﻿namespace ViewModule.controls
{
    public interface IInstanceViewControlInput : IInstanceViewControl
    {
        bool GetIsMandatory();
        string GetTextInput();
    }
}
