﻿using System.Collections.Generic;
using System.Drawing;

namespace Dungeon
{
    class CollisionHandler
    {
        private static CollisionHandler handler;

        private CollisionHandler()
        {

        }

        public static CollisionHandler Instance()
        {
            if (handler != null)
            {
                return handler;
            }
            else
            {
                return handler = new CollisionHandler();
            }
        }

        public IInstanceViewDraggableEntity IsCollidableTo(IInstanceViewCollidableEntity collidableEntity)
        {
            Rectangle actingCollidableRectangle = collidableEntity.GetBounds();
            

                /*foreach (LevelActionContainer collidableContainer in DraggableButtons)
                {
                    if (DraggableButtons.IndexOf(ContainerContext) != DraggableButtons.IndexOf(collidableContainer))
                    {
                        Rectangle collidableRectangle = collidableContainer.InstanceViewControlDraggableButton.GetBounds();
                        if (actingCollidableRectangle.IntersectsWith(collidableRectangle) == true)
                        {
                            return collidableContainer;
                        }
                    }
                }*/
            return null;
        }

    }
}
