﻿using Dungeon;
using System.Drawing;
using System.Windows.Forms;

namespace ViewModule
{
    class DragAndDropHandler
    {
        private Point ptOffset;
        private static DragAndDropHandler handler;

        private DragAndDropHandler()
        {

        }

        public static DragAndDropHandler Instance()
        {
            if (handler != null)
            {
                return handler;
            }
            else
            {
                return handler = new DragAndDropHandler();
            }
        }

        public void Drag(IInstanceViewDraggableEntity controlToDrag, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                controlToDrag.SetDrag(true);
                Point ptStartPosition = controlToDrag.PointToScreen(e.X, e.Y);

                ptOffset = new Point();
                ptOffset.X = controlToDrag.GetLocation().X - ptStartPosition.X;
                ptOffset.Y = controlToDrag.GetLocation().Y - ptStartPosition.Y;
                controlToDrag.SetLocation(new Point(ptOffset.X, ptOffset.Y));
            }
            else
            {
                controlToDrag.SetDrag(false);
            }
        }

        public void MoveAfterDrag(IInstanceViewDraggableEntity controlToDrag, MouseEventArgs e)
        {
            if (controlToDrag.GetDrag() == true)
            {
                Point newPoint = controlToDrag.PointToScreen(e.X, e.Y);
                newPoint.Offset(ptOffset);
                controlToDrag.SetLocation(new Point(newPoint.X, newPoint.Y));
            }
        }

        public void Drop(IInstanceViewDraggableEntity controlToDrag, MouseEventArgs e)
        {
            controlToDrag.SetDrag(false);
        }

    }
}
