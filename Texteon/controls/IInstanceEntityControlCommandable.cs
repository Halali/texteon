﻿using LogicModule.actionCommands;
using ViewModule.viewConditions;

namespace ViewModule.controls
{
    interface IInstanceViewControlCommandable : IInstanceViewControl
    {
        void AssignActionCommand(ICompletingActionCommand actionCommand);
        void AssignCondition(IViewCondition condition);
        void RunActionCommands();
    }
}
