﻿/*using System;
using System.Drawing;
using System.Windows.Forms;
using ViewModule;
using ViewModule.controls;

namespace Dungeon
{
    class InstanceViewControlDraggableButton : Button, IInstanceViewCollidableEntity
    {
        private Point PtOffset { get; set; }
        private bool IsDragged;

        public InstanceViewControlDraggableButton(string text="") : base()
        {
            AutoSize = true;
            this.SetText(text);
        }
        //Overrided methods
        protected override void OnMouseDown(MouseEventArgs e)
        {
            base.OnMouseDown(e);
            DragAndDropHandler.Instance().Drag(this, e);
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);
            DragAndDropHandler.Instance().MoveAfterDrag(this, e);
        }

        protected override void OnMouseUp(MouseEventArgs e)
        {
            base.OnMouseUp(e);
            DragAndDropHandler.Instance().Drop(this, e);
        }

        protected override void OnLocationChanged(EventArgs e)
        {
            base.OnLocationChanged(e);

        }

        //Accessors
        public void SetDrag(bool dragged)
        {
            IsDragged = dragged;
        }

        public bool GetDrag()
        {
            return this.IsDragged;
        }

        public void SetLocation(Point point)
        {
            this.Location = point;
        }

        public Point GetLocation()
        {
            return this.Location;
        }

        public void SetText(string text)
        {
            this.Text = text;
        }
        //Interface described methods
        public Rectangle GetBounds()
        {
            Rectangle rectangle = new Rectangle(Location.X , Location.Y, Width, Height);
            return rectangle;
        }

        public Point PointToScreen(int x, int y)
        {
            return this.PointToScreen(new Point(x, y));
        }

        public void PutOnLayout(TableLayoutPanel tableLayout)
        {
            tableLayout.Controls.Add(this);
        }

        public void ActionCommand()
        {
            MessageBox.Show("toto je z control draggable buttonu");
        }
    }
}*/
