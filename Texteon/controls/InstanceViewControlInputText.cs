﻿using System.Drawing;
using System.Windows.Forms;
using ViewModule.controls;
using ViewModule.viewForms;

namespace ViewModule.cotrols
{
    class InstanceViewControlInputText : TextBox, IInstanceViewControlInput
    {
        private bool IsMandatory { get; set; }
        new private string Name { get; set; }

        public InstanceViewControlInputText(string name="", string text="", bool isMandatory=false)
        {
            this.Name = name;
            this.IsMandatory = isMandatory;
            this.SetText(text);
        }

        public Point GetLocation()
        {
            return this.Location;
        }

        public void SetLocation(Point point)
        {
            this.Location = point;
        }

        public void SetText(string text)
        {
            this.Text = text;
        }

        public void PutOnLayout(TableLayoutPanel tableLayout)
        {
            tableLayout.Controls.Add(this);
        }

        public string GetTextInput()
        {
            return this.Text;
        }

        public void PutOnView(IStepViewForm view)
        {
            view.AssignControl(this);
        }

        public bool GetIsMandatory()
        {
            return IsMandatory;
        }

        public string GetName()
        {
            return Name;
        }

        public void SetName(string name)
        {
            Name = name;
        }
    }
}
