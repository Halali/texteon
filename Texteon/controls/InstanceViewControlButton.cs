﻿using LogicModule.actionCommands;
using LogicModule.instanceConditions;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using ViewModule.controls;
using ViewModule.viewConditions;
using ViewModule.viewForms;

namespace ViewModule.entityTypes
{
    class InstanceViewControlButton : Button, IInstanceViewControlCommandable
    {
        private List<ICompletingActionCommand> AssignedActionCommands { get; set; }
        private List<IViewCondition> AssignedConditions { get; set; }
        new private string Name { get; set; }

        public InstanceViewControlButton(string name="", string text="")
        {
            AssignedActionCommands = new List<ICompletingActionCommand>();
            AssignedConditions = new List<IViewCondition>();
            this.Name = name;
            this.SetText(text);
        }

        public Point GetLocation()
        {
            return this.Location;
        }

        public void SetLocation(Point point)
        {
            this.Location = point;
        }

        public void SetText(string text)
        {
            this.Text = text;
        }

        protected override void OnMouseClick(MouseEventArgs e)
        {
            base.OnMouseClick(e);
            RunActionCommands();
        }

        public void RunActionCommands()
        {
            if (ConditionChecker.Instance().CheckConditions(AssignedConditions))
            {
                foreach (ICompletingActionCommand ac in AssignedActionCommands)
                {
                    ac.Commit();
                }
            }
        }

        public void AssignActionCommand(ICompletingActionCommand actionCommand)
        {
            this.AssignedActionCommands.Add(actionCommand);
        }

        /*public void AssignConditionToCommand(IViewCondition condition, ICompletingActionCommand actionCommand)
        {
            AssignedActionCommands.ElementAt(AssignedActionCommands.IndexOf(actionCommand)).AssignCondition(condition);
        }*/

        public void PutOnView(IStepViewForm view)
        {
            view.AssignControl(this);
        }

        public void AssignCondition(IViewCondition condition)
        {
            AssignedConditions.Add(condition);
        }

        public string GetName()
        {
            return this.Name;
        }

        public void SetName(string name)
        {
            this.Name = name;
        }
    }
}
