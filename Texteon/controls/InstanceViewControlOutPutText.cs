﻿using System.Drawing;
using System.Windows.Forms;
using ViewModule.controls;
using ViewModule.viewForms;

namespace ViewModule.cotrols
{
    class InstanceViewControlOutputText : Label, IInstanceViewControl
    {
        new private string Name { get; set; }

        public InstanceViewControlOutputText(string name="", string text="") : base()
        {
            this.Name = name;
            this.SetText(text);
        }

        public Point GetLocation()
        {
            return this.Location;
        }

        public void SetLocation(Point point)
        {
            this.Location = point;
        }

        public void SetText(string text)
        {
            this.Text = text;
        }

        public void PutOnView(IStepViewForm view)
        {
            view.AssignControl(this);
        }

        public string GetName()
        {
            return this.Name;
        }

        public void SetName(string name)
        {
            this.Name = name;
        }
    }
}