﻿using System.Drawing;

namespace Dungeon
{
    interface IInstanceViewCollidableEntity : IInstanceViewDraggableEntity
    {
        Rectangle GetBounds();
    }
}
