﻿using ViewModule.instanceEntities;
using ViewModule.stepView;
using static ViewModule.core.ControlEnums;

namespace ViewModule.controlConteiners
{
    class StepViewConteinerFactory
    {
        private static StepViewConteinerFactory viewConteinerFactory;

        private StepViewConteinerFactory()
        {
        }

        public static StepViewConteinerFactory Instance()
        {
            if (viewConteinerFactory != null)
            {
                return viewConteinerFactory;
            }
            else
            {
                return viewConteinerFactory = new StepViewConteinerFactory();
            }
        }

        public IStepViewConteiner CreateViewConteiner(IStepView stepView, ViewConteinerType viewConteinerType = ViewConteinerType.DefaultViewConteiner)
        {
            switch (viewConteinerType)
            {
                case ViewConteinerType.DefaultViewConteiner:
                    return new StepViewConteinerDefault(stepView);
                default:
                    return new StepViewConteinerDefault(stepView);
            }
        }
    }
}
