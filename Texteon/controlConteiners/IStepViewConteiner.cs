﻿using System.Collections.Generic;
using ViewModule.controls;

namespace ViewModule.instanceEntities
{
    public interface IStepViewConteiner
    {
        List<IInstanceViewControlInput> GetInputs();
    }
}
