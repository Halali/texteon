﻿using LogicModule;
using LogicModule.actionCommands;
using System.Collections.Generic;
using ViewModule.controls;
using ViewModule.cotrols;
using ViewModule.entityTypes;
using ViewModule.stepView;
using ViewModule.viewConditions;
using ViewModule.viewForms;
using static LogicModule.core.LogicEnums;

namespace ViewModule.instanceEntities
{
    class StepViewConteinerDefault : IStepViewConteiner
    {
        private List<IInstanceViewControl> ConteinerControls { get; set; }
        private List<IInstanceViewControlInput> ConteinerInputs { get; set; }
        public List<ICompletingActionCommand> completingActionCommands;
        private IStepViewForm View { get; set; }
        private TaskManager TaskManager { get; set; }

        public StepViewConteinerDefault(IStepView view)
        {
            ConteinerControls = new List<IInstanceViewControl>();
            ConteinerInputs = new List<IInstanceViewControlInput>();
            completingActionCommands = new List<ICompletingActionCommand>();
            TaskManager = view.GetTaskManager();
            View = view.GetViewForm();

            InstanceViewControlOutputText loginTextLabel = new InstanceViewControlOutputText("Login", "LOGIN: ");
            loginTextLabel.PutOnView(View);
            ConteinerControls.Add(loginTextLabel);

            InstanceViewControlInputText loginNameTextBox = new InstanceViewControlInputText("Login", "", true);
            loginNameTextBox.PutOnView(View);
            ConteinerInputs.Add(loginNameTextBox);

            InstanceViewControlOutputText passwordTextLabel = new InstanceViewControlOutputText("Password", "PASSWORD: ");
            passwordTextLabel.PutOnView(View);
            ConteinerControls.Add(passwordTextLabel);

            InstanceViewControlInputText passwordTextBox = new InstanceViewControlInputText("Password", "", true);
            passwordTextBox.PutOnView(View);
            ConteinerInputs.Add(passwordTextBox);

            InstanceViewControlButton button = new InstanceViewControlButton("Login Button", "Login");
            ICompletingActionCommand exitInstance = ActionCommandFactory.Instance().CreateActionCommand(completingActionCommands,ActionCommandType.ExitApp);
            button.AssignActionCommand(exitInstance);
            button.AssignCondition(new ConditionAreInputsFilled(this));
            button.PutOnView(View);
            ConteinerControls.Add(button);
            
            exitInstance.Subscribe(TaskManager);
        }

        public List<IInstanceViewControlInput> GetInputs()
        {
            return ConteinerInputs;
        }
    }
}
