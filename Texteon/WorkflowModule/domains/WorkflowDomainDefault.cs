﻿using WorkflowModule.domains.workflows.instances;

namespace WorkflowModule.domains
{
    class WorkflowDomainDefault : IWorkflowDomain
    {
        public WorkflowDomainDefault()
        {
            this.RunDomain();
        }

        public void RunDomain()
        {
            WorkflowDefault defaultWorkflow = new WorkflowDefault("Login test");
            defaultWorkflow.StartWorkflow();
        }
    }
}
