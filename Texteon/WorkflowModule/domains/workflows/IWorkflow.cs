﻿using System.Threading.Tasks;

namespace WorkflowModule.domains.workflows
{
    interface IWorkflow
    {
        string GetName();
        void SetName(string name);
    }
}
