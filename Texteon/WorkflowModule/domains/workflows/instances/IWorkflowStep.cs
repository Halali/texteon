﻿using System.Threading;
using System.Threading.Tasks;

namespace WorkflowModule.domains.workflows.instances
{
    interface IWorkflowStep
    {
        Task Run(CancellationTokenSource completeToken);
        string GetName();
        void SetName(string name);
    }
}
