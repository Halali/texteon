﻿using LogicModule;
using System.Threading;
using System.Threading.Tasks;
using ViewModule.core;
using ViewModule.stepView;

namespace WorkflowModule.domains.workflows.instances
{
    class WorkflowStepDefault : IWorkflowStep
    {
        private IStepView InstanceViewLayerEntity { get; set; }
        private TaskManager TaskManager { get; set; }
        private string Name { get; set; }

        public WorkflowStepDefault(string name = "")
        {
            this.Name = name;
        }

        public async Task Run(CancellationTokenSource completeToken)
        {
            TaskManager = new TaskManager(completeToken);
            InstanceViewLayerEntity = StepViewFactory.Instance().CreateViewEntity(TaskManager);
            InstanceViewLayerEntity.ShowView();
        }

        public void SetName(string name)
        {
            this.Name = name;
        }

        public string GetName()
        {
            return this.Name;
        }
    }
}
