﻿using static WorkflowModule.WorkflowEnums;

namespace WorkflowModule.domains.workflows.instances
{
    class WorkflowStepFactory
    {
        private static WorkflowStepFactory stepFactory;

        private WorkflowStepFactory()
        {
        }

        public static WorkflowStepFactory Instance()
        {
            if (stepFactory != null)
            {
                return stepFactory;
            }
            else
            {
                return stepFactory = new WorkflowStepFactory();
            }
        }

        public IWorkflowStep CreateStep(StepType stepType = StepType.DefaultStep)
        {
            switch (stepType)
            {
                case StepType.DefaultStep:
                    return new WorkflowStepDefault("Login screen");
                default:
                    return new WorkflowStepDefault();
            }
        }

    }
}
