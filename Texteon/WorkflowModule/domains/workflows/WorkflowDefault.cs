﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WorkflowModule.domains.workflows.instances
{
    class WorkflowDefault : IWorkflow
    {
        private CancellationTokenSource completeToken;
        private List<IWorkflowStep> workflowSteps;
        private string Name { get; set; }

        public WorkflowDefault(string name="")
        {
            this.Name = name;
            workflowSteps = new List<IWorkflowStep>();
            workflowSteps.Add(WorkflowStepFactory.Instance().CreateStep());
        }

        public async void StartWorkflow()
        {
            foreach(IWorkflowStep step in workflowSteps)
            {
                completeToken = new CancellationTokenSource();

                try
                {
                    await RunStep(step);
                    RunStep(step).Wait(completeToken.Token);
                }
                catch (OperationCanceledException)
                {
                    MessageBox.Show("Step " + step.GetName() + " was finished");
                    if(workflowSteps.IndexOf(step) + 1 == workflowSteps.Count)
                        MessageBox.Show("Workflow " + this.GetName() + " was finished");
                }
                catch (Exception)
                {
                    MessageBox.Show("Operation failed");
                }

                completeToken = null;
            }
        }

        private async Task RunStep(IWorkflowStep stepToRun)
        {
            await stepToRun.Run(completeToken);
        }

        public string GetName()
        {
            return this.Name;
        }

        public void SetName(string name)
        {
            this.Name = name;
        }
    }
}
