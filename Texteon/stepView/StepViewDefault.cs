﻿using ViewModule.controlConteiners;
using ViewModule.instanceEntities;
using static ViewModule.core.ControlEnums;
using LogicModule;
using ViewModule.viewForms;

namespace ViewModule.stepView
{
    public class StepViewDefault : IStepView
    {
        internal IStepViewForm StepViewForm { get; set; }
        internal IStepViewConteiner InstanceViewConteiner { get; set; }
        internal TaskManager TaskManager { get; set; }

        /// <summary>
        /// View for step with default Form and conteiner with controls assigned to view
        /// </summary>
        /// <param name="view"></param>
        /// <param name="conteinerType"></param>
        public StepViewDefault(TaskManager taskManager, ViewType view = ViewType.DefaultView, ViewConteinerType conteinerType = ViewConteinerType.DefaultViewConteiner)
        {
            TaskManager = taskManager;
            StepViewForm = StepViewFormFactory.Instance().CreateViewForm();
            InstanceViewConteiner = StepViewConteinerFactory.Instance().CreateViewConteiner(this);
        }

        public void ShowView()
        {
            StepViewForm.ShowForm();
        }

        public IStepViewConteiner GetControlConteiner()
        {
            return InstanceViewConteiner;
        }

        public IStepViewForm GetViewForm()
        {
            return StepViewForm;
        }

        public TaskManager GetTaskManager()
        {
            return TaskManager;
        }
    }
}
