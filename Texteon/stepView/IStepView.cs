﻿using LogicModule;
using ViewModule.instanceEntities;
using ViewModule.viewForms;

namespace ViewModule.stepView
{
    public interface IStepView
    {
        IStepViewConteiner GetControlConteiner();
        TaskManager GetTaskManager();
        IStepViewForm GetViewForm();
        void ShowView();
    }
}
