﻿namespace ViewModule.core
{
    public static class ControlEnums
    {
        public enum ViewType
        {
            DefaultView
        }

        public enum ViewConteinerType
        {
            DefaultViewConteiner
        }

        public enum ViewEntityType
        {
            DefaultViewEntity
        }
    }
}
