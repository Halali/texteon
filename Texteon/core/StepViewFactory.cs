﻿using LogicModule;
using ViewModule.stepView;
using static ViewModule.core.ControlEnums;

namespace ViewModule.core
{
    public class StepViewFactory
    {
        private static StepViewFactory viewEntityFactory;

        private StepViewFactory()
        {
        }

        public static StepViewFactory Instance()
        {
            if (viewEntityFactory != null)
            {
                return viewEntityFactory;
            }
            else
            {
                return viewEntityFactory = new StepViewFactory();
            }
        }

        public IStepView CreateViewEntity(TaskManager taskManager, ViewEntityType ViewEntityType = ViewEntityType.DefaultViewEntity)
        {
            switch (ViewEntityType)
            {
                case ViewEntityType.DefaultViewEntity:
                    return new StepViewDefault(taskManager);
                default:
                    return new StepViewDefault(taskManager);
            }
        }
    }
}
