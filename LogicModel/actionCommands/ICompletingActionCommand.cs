﻿using System;

namespace LogicModule.actionCommands
{
    public interface ICompletingActionCommand : IActionCommand
    {
        bool IsCompleted();
        IDisposable Subscribe(IObserver<ICompletingActionCommand> observer);
    }
}
