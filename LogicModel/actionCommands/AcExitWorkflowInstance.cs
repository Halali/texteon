﻿using System;
using System.Collections.Generic;

namespace LogicModule.actionCommands
{
    public class AcExitWorkflowInstance : ICompletingActionCommand, IObservable<ICompletingActionCommand>
    {
        private List<ICompletingActionCommand> ActionCommands { get; set; }
        private List<IObserver<ICompletingActionCommand>> Observers { get; set; }
        private bool Finished { get; set; }

        public AcExitWorkflowInstance(List<ICompletingActionCommand> acs)
        {
            Finished = false;
            Observers = new List<IObserver<ICompletingActionCommand>>();
            acs.Add(this);
            ActionCommands = acs;
        }

        public void Commit()
        {
            Finished = true;
            foreach (var observer in Observers.ToArray())
                if (observer != null) observer.OnCompleted();
        }

        public IDisposable Subscribe(IObserver<ICompletingActionCommand> observer)
        {
            if (!Observers.Contains(observer))
            {
                Observers.Add(observer);
                foreach (var item in ActionCommands)
                    observer.OnNext(item);
            }
            return new Unsubscriber<ICompletingActionCommand>(Observers, observer);
        }

        public bool IsCompleted()
        {
            return Finished;
        }

    }

    internal class Unsubscriber<IActionCommand> : IDisposable
    {
        private List<IObserver<IActionCommand>> observers;
        private IObserver<IActionCommand> observer;

        public Unsubscriber(List<IObserver<IActionCommand>> observersToUnSub, IObserver<IActionCommand> observerToUnSub)
        {
            this.observers = observersToUnSub;
            this.observer = observerToUnSub;
        }

        public void Dispose()
        {
            if (observers.Contains(observer))
                observers.Remove(observer);
        }
    }
}
