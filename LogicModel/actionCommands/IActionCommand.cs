﻿namespace LogicModule.actionCommands
{
    public interface IActionCommand
    {
        void Commit();
    }
}
