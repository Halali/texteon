﻿using System.Collections.Generic;
using static LogicModule.core.LogicEnums;

namespace LogicModule.actionCommands
{
    public class ActionCommandFactory
    {
        private static ActionCommandFactory actionCommandFactory;

        private ActionCommandFactory()
        {
        }

        public static ActionCommandFactory Instance()
        {
            if (actionCommandFactory != null)
            {
                return actionCommandFactory;
            }
            else
            {
                return actionCommandFactory = new ActionCommandFactory();
            }
        }

        public ICompletingActionCommand CreateActionCommand(List<ICompletingActionCommand> actionCommands, ActionCommandType commandType)
        {
            switch (commandType)
            {
                case ActionCommandType.ExitApp:
                    return new AcExitWorkflowInstance(actionCommands);
                default:
                    return new AcExitWorkflowInstance(actionCommands);
            }
        }
    }
}
