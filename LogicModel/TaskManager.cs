﻿using LogicModule.actionCommands;
using System.Collections.Generic;
using System.Threading;
using System;

namespace LogicModule
{
    public class TaskManager : IObserver<ICompletingActionCommand>
    {
        private List<ICompletingActionCommand> ActionCommands { get; set; }
        private CancellationTokenSource CancellationTokenSource { get; set; }
        private IDisposable cancellation;

        public TaskManager(CancellationTokenSource cancellationTokenSource)
        {
            CancellationTokenSource = cancellationTokenSource;
            ActionCommands = new List<ICompletingActionCommand>();
        }

        public bool AllTasksFinished()
        {
            foreach (ICompletingActionCommand ac in ActionCommands)
            {
                if (!ac.IsCompleted())
                {
                    return false;
                }
            }
            return true;
        }

        public virtual void Subscribe(ICompletingActionCommand provider)
        {
            cancellation = provider.Subscribe(this);
        }

        public virtual void Unsubscribe()
        {
            cancellation.Dispose();
        }

        public void OnCompleted()
        {
            if (this.AllTasksFinished() == true)
            {
                CancellationTokenSource.Cancel();
                CancellationTokenSource.Token.ThrowIfCancellationRequested();
            }
        }

        public void OnError(Exception error)
        {
            
        }

        public void OnNext(ICompletingActionCommand ac)
        {
            ActionCommands.Add(ac);
        }
    }
}
