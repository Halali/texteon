﻿namespace LogicModule.core
{
    public static class LogicEnums
    {
        public enum ActionCommandType
        {
            ExitApp
        }

        public enum ConditionType
        {
            IsExitButtonClicked
        }

        public enum BehaviorSet
        {
            Basic
        }

    }
}
