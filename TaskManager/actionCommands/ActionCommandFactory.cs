﻿using LogicModule.instanceConditions;
using System.Collections.Generic;
using System.Threading.Tasks;
using static LogicModule.core.LogicEnums;

namespace TaskManager.actionCommands
{
    public class ActionCommandFactory
    {
        private static ActionCommandFactory actionCommandFactory;

        private ActionCommandFactory()
        {
        }

        public static ActionCommandFactory Instance()
        {
            if (actionCommandFactory != null)
            {
                return actionCommandFactory;
            }
            else
            {
                return actionCommandFactory = new ActionCommandFactory();
            }
        }

        public IActionCommand CreateActionCommand(ActionCommandType commandType)
        {
            switch (commandType)
            {
                case ActionCommandType.ExitApp:
                    return new AcExitWorkflowInstance();
                default:
                    return new AcExitWorkflowInstance();
            }
        }
    }
}
