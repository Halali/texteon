﻿using System;
using System.Collections.Generic;
using TaskManager.instanceConditions;

namespace TaskManager.actionCommands
{
    public class AcExitWorkflowInstance : IActionCommand, IObservable<IInstanceCondition>
    {
        private List<IInstanceCondition> Conditions { get; set; }
        private List<IObserver<IInstanceCondition>> Observers { get; set; }

        public AcExitWorkflowInstance()
        {
            Observers = new List<IObserver<IInstanceCondition>>();
            Conditions = new List<IInstanceCondition>();
        }

        public void AssignCondition(IInstanceCondition condition)
        {
            this.Conditions.Add(condition);
        }

        public bool CheckConditions()
        {
            return false;
        }

        public void Commit()
        {
            
            foreach (var observer in Observers.ToArray())
                if (observer != null) observer.OnCompleted();
        }

        public IDisposable Subscribe(IObserver<IInstanceCondition> observer)
        {
            if (!Observers.Contains(observer))
            {
                Observers.Add(observer);
                foreach (var item in Conditions)
                    observer.OnNext(item);
            }
            return new Unsubscriber<IInstanceCondition>(Observers, observer);
        }
    }

    public class Unsubscriber<IInstanceCondition> : IDisposable
    {
        private List<IObserver<IInstanceCondition>> _observers;
        private IObserver<IInstanceCondition> _observer;

        public Unsubscriber(List<IObserver<IInstanceCondition>> observers, IObserver<IInstanceCondition> observer)
        {
            this._observers = observers;
            this._observer = observer;
        }

        public void Dispose()
        {
            if (_observers.Contains(_observer))
                _observers.Remove(_observer);
        }
    }








}
