﻿using LogicModule.instanceConditions;
using System;

namespace TaskManager.actionCommands
{
    public interface IActionCommand
    {
        void AssignCondition(IInstanceCondition condition);
        bool CheckConditions();
        void Commit();
        IDisposable Subscribe(IObserver<IInstanceCondition> observer);
    }
}
