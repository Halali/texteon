﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using TaskManager.actionCommands;
using TaskManager.instanceConditions;

namespace TaskManager
{
    public class TaskManager : IObserver<IInstanceCondition>
    {
        private static List<IInstanceCondition> firstInstanceConditions;
        private IDisposable cancellation;
        private static int numberOfConditions;
        private static int filledConditions;

        public TaskManager(int conditionsNumber)
        {
            numberOfConditions = conditionsNumber;
            filledConditions = 0;
            //firstInstanceConditions = conditions;
        }

        public static async Task ConditionChecker()
        {
            await Task.Run(()=> {
                Thread.Sleep(3000);
                if(numberOfConditions == filledConditions)
                {
                    MessageBox.Show("Completed");
                }
                /*foreach (IInstanceCondition c in firstInstanceConditions)
                {
                    if (c.Check() == true)
                    {
                        c.True();
                    }
                }*/
            });
        }

        public virtual void Subscribe(AcExitWorkflowInstance provider)
        {
            cancellation = provider.Subscribe(this);
        }

        public virtual void Unsubscribe()
        {
            cancellation.Dispose();
            firstInstanceConditions.Clear();
        }

        public void OnCompleted()
        {
            filledConditions += 1;
            Task.Run(() => ConditionChecker());
            firstInstanceConditions.Clear();
            
        }

        public void OnError(Exception error)
        {
            
        }

        public void OnNext(IInstanceCondition value)
        {
            Task.Run(()=> ConditionChecker());
        }
    }
}
