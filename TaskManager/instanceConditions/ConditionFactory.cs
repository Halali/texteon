﻿using TaskManager.instanceConditions;
using static LogicModule.core.LogicEnums;

namespace TaskManager.actionCommands
{
    public class ConditionFactory
    {
        private static ConditionFactory conditionFactory;

        private ConditionFactory()
        {
        }

        public static ConditionFactory Instance()
        {
            if (conditionFactory != null)
            {
                return conditionFactory;
            }
            else
            {
                return conditionFactory = new ConditionFactory();
            }
        }

        public IInstanceCondition CreateCondition(ConditionType conditionType)
        {
            switch (conditionType)
            {
                case ConditionType.IsExitButtonClicked:
                    return new ConditionExitButtonClicked();
                default:
                    return new ConditionExitButtonClicked();
            }
        }
    }
}
