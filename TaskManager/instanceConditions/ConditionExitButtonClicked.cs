﻿namespace TaskManager.instanceConditions
{
    public class ConditionExitButtonClicked : IInstanceCondition
    {
        private bool isTrue;

        public ConditionExitButtonClicked()
        {
            isTrue = false;
        }

        public bool Check()
        {
            if (isTrue == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void True()
        {
            isTrue = true;
        }
    }
}