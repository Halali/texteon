﻿using System.ComponentModel;

namespace TaskManager.instanceConditions
{
    public interface IInstanceCondition
    {
        /// <summary>
        /// Set this condition true
        /// </summary>
        void True();
        /// <summary>
        /// Check if condition is true
        /// </summary>
        bool Check();
    }
}
